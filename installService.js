var Service = require('node-windows').Service;

// Create a new service object
var svc = new Service({
  name:'Barak Installer',
  description: 'API for latest barak version and configuration file',
  script: 'C:\\Pixel\\barak-installer\\index.js',
  env: {
      name: "NODE_ENV",
      value: "development"
  }
});

// Listen for the "install" event, which indicates the
// process is available as a service.
svc.on('install',function(){
  svc.start();
});

svc.install();
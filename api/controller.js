"use strict";
const config = require("config");
const fs = require("fs");
const path = require("path");
const semver = require("semver");

let getLatestAppVersion = async function(req, res) {
    try {
        let version = req.query.version;
        let latestVersion = version;
        let versionPathToSend = "";
        fs.readdirSync(config.appVersionsFolderPath).forEach( file => {
            var currVersion = path.basename(file,path.extname(file)); //Assuming file names is their semver version
            if (semver.gt(currVersion,latestVersion)) {
                latestVersion = currVersion;
                versionPathToSend = path.join(config.appVersionsFolderPath, file);
            }
        });

        if (latestVersion <= version) {
            return res.status(404).send(`Newer Barak version was not found.`);
        }

        return res.status(200).sendFile(versionPathToSend);
    } catch(err) {
        return res.status(500).send(err.message);
    }
}

let getLatestConfigVersion = async function(req, res) {
    try {
        let latestConfigJsonPath = config.latestConfigJsonPath;
        return res.status(200).sendFile(latestConfigJsonPath);
    } catch(err) {
        return res.status(500).send(err.message);
    }
}

module.exports = { getLatestAppVersion, getLatestConfigVersion};

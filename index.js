"use strict";
const config = require('config');
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const swaggerUi = require('swagger-ui-express');
const YAML = require('yamljs');
const swaggerDocument = YAML.load('./api/swagger.yaml');

const { connector } = require('swagger-routes-express');
const api = require('./api/controller');

// TO CONFIG
const port = process.env.PORT || 3000;

app.use(bodyParser.urlencoded({extended: false}));
app.use(express.json());
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
const connect = connector(api, swaggerDocument);
connect(app);
app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`));